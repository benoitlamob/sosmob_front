# formForFilterByTech

## Props

<!-- @vuese:formForFilterByTech:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|usersDisplay|-|—|`false`|-|
|listTechs|-|—|`false`|-|

<!-- @vuese:formForFilterByTech:props:end -->


## Events

<!-- @vuese:formForFilterByTech:events:start -->
|Event Name|Description|Parameters|
|---|---|---|
|loadUsersForTechRequest|-|-|

<!-- @vuese:formForFilterByTech:events:end -->


