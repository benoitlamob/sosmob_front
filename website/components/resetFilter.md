# resetFilter

## Props

<!-- @vuese:resetFilter:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|reset|-|—|`false`|-|

<!-- @vuese:resetFilter:props:end -->


