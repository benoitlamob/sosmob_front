# filterCityClient

## Props

<!-- @vuese:filterCityClient:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|citiesDisplay|-|—|`false`|-|
|clientsDisplay|-|—|`false`|-|

<!-- @vuese:filterCityClient:props:end -->


## Events

<!-- @vuese:filterCityClient:events:start -->
|Event Name|Description|Parameters|
|---|---|---|
|filterUsersByCityAndClient|-|-|
|loadCitiesAndClientsFromUsers|-|-|

<!-- @vuese:filterCityClient:events:end -->


