# listMissionUser

## Props

<!-- @vuese:listMissionUser:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|missionsUser|-|—|`false`|-|
|techsMission|-|—|`false`|-|
|listTechs|-|—|`false`|-|
|udaptePermission|-|—|`false`|-|

<!-- @vuese:listMissionUser:props:end -->


