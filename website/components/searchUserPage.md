# searchUserPage

This is the component Root of SearchUser and Display ListUser

## Methods

<!-- @vuese:searchUserPage:methods:start -->
|Method|Description|Parameters|
|---|---|---|
|loadUsers|Load User from the API and store loaded data into local data. Expects a baerer idToken.|-|
|filterUserByTech|Filter UserDisplay by Tech|tech is a string of a tech|
|filterList|Generic method able to filter userList from an element source is a string element of the filter, usersFilter is the array of user used in the filter process|filterElement is the array of element filter,|
|filterUsersByCityAndClient|Filter UserDisplay by Clients or Cities clients is an array of client|cities is an array of city,|
|loadCitiesAndClientsFromUsers|it's a dynamic function able to load cities or users from list of users|-|
|loadTechs|load techs from techs db and store into local array of techs|-|

<!-- @vuese:searchUserPage:methods:end -->


