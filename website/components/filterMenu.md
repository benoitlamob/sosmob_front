# filterMenu

## Props

<!-- @vuese:filterMenu:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|usersDisplay|-|—|`false`|-|
|usersData|-|—|`false`|-|
|citiesDisplay|-|—|`false`|-|
|clientsDisplay|-|—|`false`|-|
|listTechs|-|—|`false`|-|

<!-- @vuese:filterMenu:props:end -->


## Events

<!-- @vuese:filterMenu:events:start -->
|Event Name|Description|Parameters|
|---|---|---|
|loadUsers|-|-|
|filterUserByTech|-|-|
|loadCitiesAndClientsFromUsers|-|-|

<!-- @vuese:filterMenu:events:end -->


