import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import VCalendar from 'v-calendar';
import { library } from '@fortawesome/fontawesome-svg-core'
import { faFilter, faUserCircle, faLongArrowAltLeft, faPencilAlt, faPlusSquare} from '@fortawesome/free-solid-svg-icons'
import { faVuejs, faAngular, faReact, faNodeJs } from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import router from './config/router'
import GAuth from 'vue-google-oauth2'
import {gauthOption} from './auth/config'

library.add(faFilter, faUserCircle, faLongArrowAltLeft, faVuejs, faAngular, faReact, faNodeJs, faPencilAlt, faPlusSquare)
Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.use(GAuth, gauthOption)
Vue.use(BootstrapVue)
Vue.use(VueRouter)
Vue.use(VCalendar,{
  locales:{'en-US':{
    firstDayOfWeek: 1,
    masks: {
      L: 'YYYY-MM-DD',
      // ...optional `title`, `weekdays`, `navMonths`, etc
    }
  }
  }
})

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  router,
}). $mount('#app')