import axios from 'axios'
import {url} from '../config/environment'
import {configAuth, configHeader} from './config'

export const checkUserAuth = async () =>{
    if(localStorage.getItem('id_token')){
        if(localStorage.getItem('access_token')){
            try{
                await checkAccessTokenValidity()
                const response = await getMe()
                if(response.status == '200'){
                    localStorage.setItem('id_user', response.data.id)
                    return true
                }else if(response.status == '404'){
                    return response.data
                }else{
                    localStorage.clear()
                    return false
                }
            }catch(error){
                if(localStorage.getItem('refresh_token')){
                    try{
                        await refreshTokens()
                    }catch(error){
                        throw error
                    }
                }else {
                    throw error
                }
            }
        }else{
            if(localStorage.getItem('refresh_token')){
                try{
                    await refreshTokens()
                }catch(error){
                    signOut()
                    throw error
                }
            }else {
                signOut()
                throw error
            }
        }
    }else{
        return false
    }
}

export const checkAccessTokenValidity = async () => {
    try{
        let accessToken = localStorage.getItem('access_token')
        const response = await axios.get(`https://www.googleapis.com/oauth2/v1/tokeninfo?access_token=${accessToken}`)
        if(response.status === 200 && response.data.user_id !== undefined){ 
            return true
        }
    } catch(error){
        throw error
    }
}

export const refreshTokens = async () => {
    try{
        let refreshToken = localStorage.getItem('refresh_token')
        const response = await axios.post(`https://www.googleapis.com/oauth2/v4/token`, {
            client_secret: configAuth.client_secret,
            grant_type: 'refresh_token',
            refresh_token: refreshToken,
            client_id: configAuth.clientID
        })
        if(response.data.id_token){
            localStorage.setItem('access_token', response.data.access_token)
            localStorage.setItem('id_token', response.data.id_token)
            await checkUserAuth()
        }
    } catch(error){
        signOut()
        throw error
    }
}

export const getTokensFromCodeAuth = async (authCode) => {
    try{
        const response = await axios.post(`${configAuth.url_codeAuth}`, {
            code: authCode, 
            client_id: configAuth.clientID, 
            client_secret: configAuth.client_secret,
            redirect_uri: configAuth.redirect_uri,
            grant_type: 'authorization_code',
            prompt:'consent',
            access_type:'offline'
        })
        if(response.data.access_token){
            localStorage.setItem('refresh_token', response.data.refresh_token)
            localStorage.setItem('access_token', response.data.access_token)
            localStorage.setItem('id_token', response.data.id_token)
        }
    } catch(error){
        throw error
    }
}

export const getMe = async () =>{
    let response
    let errorStatus
    try{
        if(localStorage.getItem('id_token')){
            response = await axios.get(`${url}/me`, configHeader())
        }
        }catch(err){
            response = err.response
        }finally{
            if (!response) {
                errorStatus = 'Error: Network Error'
                return errorStatus
            } else{
                return response
            }
        }
    }
  

export const signOut = async () => {
    const accessToken = localStorage.getItem('access_token')
    //revoke Access
    await axios.get(`https://accounts.google.com/o/oauth2/revoke?token=${accessToken}`)
    localStorage.removeItem('refresh_token')
    localStorage.removeItem('access_token')
    localStorage.removeItem('id_token')
    localStorage.removeItem('id_user')
    localStorage.setItem('auth', true)
}

export const inscription = async (user) =>{
    try{
        await axios.post(`${url}/users`, user, configHeader())
    }catch(error){
        throw error
    }
}