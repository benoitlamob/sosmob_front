// fonction permettant de filter les users d'un list par rapport à un element donné 
export const filterList = (filterElement, filterSource, usersFilter) => {
    let userTransition = []
    for(let userFilter of usersFilter){
        filterElement.forEach(element =>{
            if(userFilter[filterSource] == element){
                userTransition.push(userFilter)
            }
        })
    }
    return userTransition
}

export const getElementsFromUsers = (element, users) =>{
    let elementsFromUsers = []
    for(let userData of users){
        let hasToBePushed = true
        elementsFromUsers.forEach(data => {
            if(data == userData[element]){
                hasToBePushed = false
            }
        })
        if(hasToBePushed){
            elementsFromUsers.push(userData.city)
        }  
    }
    return elementsFromUsers
}