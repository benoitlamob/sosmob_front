import axios from 'axios'

export const loadIdSlack = async (email) => {
    const index = email.indexOf("@")
    let idLoginSlack = email.slice(0, index)
    const response = await axios.get(
        'https://slack.com/api/users.list?token=xoxp-276752139170-734179124083-755712638004-fa7e983bc8db0d73f2f3faa6776a583b&pretty=1/')
    const idUserSlack = response.data.members.filter(member => member.name == idLoginSlack)[0].id
    return idUserSlack
}