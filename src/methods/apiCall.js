import axios from 'axios'
import {url} from '../config/environment'
import {configHeader} from '../auth/config'

export const loadTechs = async () =>{
    try{
        const response = await axios.get(`${url}/techs`, configHeader())
        return response.data
    }catch(error){
        throw error
    }
    
}