import VueRouter from 'vue-router'
import login from '../components/login/login.vue'
import searchUserPage from '../components/layout/searchUserPage.vue'
import notFound from '../components/404notfound/notFound.vue'
import profileUserPage from '../components/layout/profileUserPage.vue'
import {checkUserAuth} from '../auth'


const routes = [
    { path: '/login', component: login, name: 'login', meta: { requiresAuth : false }},
    { path: '/', component: searchUserPage, name: 'home', meta: { requiresAuth : true }},
    { path: '/profile/:email', component: profileUserPage, name: 'profile', props: true, meta: { requiresAuth : true }},
    { path: '/notfound', component: notFound, meta: { requiresAuth : false }},
    { path: '*',redirect: '/notfound'},
    { path: '/logout', redirect: '/login'}
  ]
    
const router = new VueRouter({
    routes 
  })

router.beforeEach(async (to, from, next) =>{
    if(!(await checkUserAuth()) && to.matched.some(record => record.meta.requiresAuth)){
        return next({name: 'login'})
    }
    if((await checkUserAuth()) && to.path === '/login'){
        return next({name: 'home'})
    }
    return next()
})

export default router