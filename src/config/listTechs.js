export const listTechs = [
    {
        name: "React",
        icon: "react"
    },
    {
        name: "Node",
        icon: "node-js"
    },
    {
        name: 'Vue',
        icon: "vuejs"
    },
    {
        name: 'Angular',
        icon: "angular"
    }
]